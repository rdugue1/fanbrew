# Fanbrew API 
This is the [Graphql](https://graphql.org/) API for what will eventually become a social media platform for 
people interested in creating their own fantasy content. This project uses the [AWS CDK](https://aws.amazon.com/cdk/)
for Typescript to define the architecture, and within that architecture is an AWS Lambda function that hosts the 
[Apollo Server](https://www.apollographql.com/docs/apollo-server/).

## Architecture 
The architecture is serverless and consists of several AWS resources: 



## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template

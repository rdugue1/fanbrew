import 'reflect-metadata';
import { Service } from 'typedi';
import { Pool } from 'pg'


@Service()
export class FanbrewRDS {

    pool = new Pool({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        database: process.env.DB_NAME,
        password: process.env.DB_PASSWORD,
        port: parseInt(process.env.DB_PORT || "5432")
    });


}
import { Stack, StackProps, RemovalPolicy, Duration } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as rds from 'aws-cdk-lib/aws-rds';
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as apigw from 'aws-cdk-lib/aws-apigateway';
import * as elbv2 from 'aws-cdk-lib/aws-elasticloadbalancingv2';
import * as iam from 'aws-cdk-lib/aws-iam';
import * as cognito from 'aws-cdk-lib/aws-cognito';

export class FanbrewStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    // Create the VPC
    const vpc = new ec2.Vpc(this, 'FanbrewVpc', {
      cidr: '10.0.0.0/16',
      natGateways: 1,
      subnetConfiguration: [
        {
          cidrMask: 24,
          name: 'public',
          subnetType: ec2.SubnetType.PUBLIC,
        },
        {
          cidrMask: 24,
          name: 'private',
          subnetType: ec2.SubnetType.PRIVATE_ISOLATED
        }
      ],
    });

    // Create the load balancer 
    const nlb = new elbv2.NetworkLoadBalancer(this, 'NLB', {
      vpc,
      vpcSubnets: {
        subnets: vpc.publicSubnets,
        subnetType: ec2.SubnetType.PUBLIC
      }
    });

    // Create username and password secret for DB Cluster
    const secret = new rds.DatabaseSecret(this, 'AuroraSecret', {
      username: 'fanbrewAdmin',
    });

    /// Create the database 
    const fanbrewDatabase = new rds.ServerlessCluster(this, 'FanbrewDatabase', {
      engine: rds.DatabaseClusterEngine.AURORA_POSTGRESQL,
      vpc,
      vpcSubnets: {
        subnets: vpc.privateSubnets,
        subnetType: ec2.SubnetType.PRIVATE_ISOLATED
      },
      enableDataApi: true,
      removalPolicy: RemovalPolicy.RETAIN,
      defaultDatabaseName: 'FanbrewDatabase',
      credentials: {
        username: 'fanbrewAdmin',
        password: secret?.secretValue
      }
    });

    // Create the lambda function for the graphql backend 
    const fanbrewBackend = new lambda.Function(this, 'FanbrewBackend', {
      code: lambda.Code.fromAsset('dist'),
      handler: 'index.handler',
      runtime: lambda.Runtime.NODEJS_14_X,
      vpc,
      vpcSubnets: {
        subnets: vpc.privateSubnets,
        subnetType: ec2.SubnetType.PRIVATE_ISOLATED
      },
      environment: {
        'dbClusterArn': fanbrewDatabase.clusterArn,
        'DB_HOST': fanbrewDatabase.clusterEndpoint.hostname,
        'DB_PORT': fanbrewDatabase.clusterEndpoint.port.toString(),
        'DB_NAME': 'FanbrewDatabase',
        'DB_USER': 'admin',
        'DB_PASSWORD': secret?.secretValue.toString()
      },
      timeout: Duration.seconds(30)
    });

    // Give database access to the lambda function 
    fanbrewBackend.addToRolePolicy(new iam.PolicyStatement({
      actions: ["rds:*"],
      resources: [fanbrewDatabase.clusterArn]
    }))
    fanbrewDatabase.grantDataApiAccess(fanbrewBackend)
    fanbrewDatabase.connections.allowFrom(fanbrewBackend, ec2.Port.tcp(fanbrewDatabase.clusterEndpoint.port))

    // Create the S3 bucket for media 
    const fanbrewStorage = new s3.Bucket(this, 'FanbrewStorage', {
      versioned: true,
    });

    // Create vpc endpoint and restrict bucket to only accept requests from it
    const endpoint = new ec2.CfnVPCEndpoint(this, 'FanbrewS3Endpoint', {
      vpcId: vpc.vpcId, // Replace "vpc" with the name of your VPC
      serviceName: 'com.amazonaws.us-east-1.s3',
      routeTableIds: vpc.privateSubnets.map(subnet => subnet.routeTable.routeTableId),
    });
    fanbrewStorage.addToResourcePolicy(new iam.PolicyStatement({
      actions: ["s3:*"],
      resources: [fanbrewStorage.arnForObjects("*")],
      conditions: {
        "StringEquals": {
          "aws:sourceVpce": endpoint.ref, // Replace with the ID of your VPC endpoint
        },
      }
    }));

    // Create the API Gateway
    const fanbrewApi = new apigw.RestApi(this, 'FanbrewApi', {
      restApiName: 'Fanbrew API',
    });


    const userPool = new cognito.UserPool(this, 'FanbrewUserPool')

    // Create cognito authorizer
    const auth = new apigw.CognitoUserPoolsAuthorizer(this, 'FanbrewAuth', {
      cognitoUserPools: [userPool]
    })

    // Create a VPC link for the API Gateway to enable it to communicate with the Lambda function in the private subnet
    const vpcLink = new apigw.VpcLink(this, 'MyVpcLink', {
      targets: [nlb],
    });

    const fanbrewResource = fanbrewApi.root.addResource('fanbrewBackend');
    fanbrewResource.addMethod('GET', new apigw.LambdaIntegration(fanbrewBackend, { vpcLink, }), {
      apiKeyRequired: false,
      authorizer: auth,
      authorizationType: apigw.AuthorizationType.COGNITO
    });
  }
}
